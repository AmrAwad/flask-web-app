from flask import Flask
from flask_bootstrap import Bootstrap

def create_app():
    app = Flask(__name__)
    Bootstrap(app)

    app.config['UPLOAD_FOLDER'] = 'uploads'
    
    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    return app
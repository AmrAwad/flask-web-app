from flask import Blueprint, request, jsonify, abort, render_template
from flask import current_app as app

from werkzeug import secure_filename
import io
import os

main = Blueprint('main', __name__)

@main.route('/')
def home():
	return render_template("index.html")

@main.route('/upload', methods=['POST'])
def upload():
	if 'file' in request.files:
		if not os.path.exists(app.config['UPLOAD_FOLDER']):
			os.makedirs(app.config['UPLOAD_FOLDER'])
		file = request.files['file']
		file_name = secure_filename(file.filename)
		filepath = os.path.join(app.config['UPLOAD_FOLDER'], file_name)
		file_content = file.stream.read().decode('utf-8')
		file.save(filepath)
		return jsonify({"file_name": file_name, "file_content": file_content})
	abort(404)

@main.route('/display', methods=['GET'])
def display():
	return jsonify({"status": "ok", "content": "here is the content"})